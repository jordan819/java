package com.company;

public class Main {

    static int calculateSum(int n) {
        int sum = 0;
        for (int i=0; i<n; i++) {
            sum += i;
        }
        return sum;
    }
    
    public static int calculateSumOfNaturalNumbers(int number){
        if (number!=0){
            return  number + calculateSumOfNaturalNumbers(number - 1);
        } else {
            return number;
        }
    }

    public static void main(String[] args) {

        System.out.print("Suma iteracyjnie: " + calculateSum(5));

    }
}
